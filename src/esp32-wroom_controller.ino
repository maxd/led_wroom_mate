#include <SPI.h>
// #include <Ethernet3.h>
// #include <EthernetUdp3.h>
// #include <SimpleDDP.h>

#define FASTLED_ESP32_I2S true
#include <FastLED.h>
#include <Shadel.h>

#include <ESParam.h>
#include <ESParamPrinter.h>

#include <ParamUtils.h>
#include <Bounce2.h>

byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(10, 0, 0, 32);
IPAddress subnet(255,255,255,0);
IPAddress gateway(10,0,0,1);
IPAddress dnsServer(10,0,0,1);


// BOARD SETUP
#define HSPI_MISO 12
#define HSPI_MOSI 13
#define HSPI_SCLK 14
#define HSPI_CS   15
#define WIZ_RESET_PIN 3
// SPIClass * hspi = NULL;
#define OLED_SDA_PIN 21
#define OLED_SCL_PIN 22

#define POT_A_PIN    39
#define POT_B_PIN    34
#define POT_C_PIN    35
#define BUT_A_PIN    25
#define BUT_B_PIN    33
#define BUT_C_PIN    32

#define LED_PIN 2

// EthernetUDP Udp;




#define LED_DATA_1_PIN 23
#define LED_DATA_2_PIN 19
#define LED_DATA_3_PIN 18
#define LED_DATA_4_PIN 17
// #define LED_DATA_5_PIN 16
// #define LED_DATA_6_PIN 4
// #define LED_DATA_7_PIN 2
// #define LED_DATA_8_PIN 25
// #define LED_DATA_9_PIN 32
// #define LED_DATA_10_PIN 33
// #define LED_DATA_11_PIN 26
// #define LED_DATA_12_PIN 27

#define LED_TYPE    WS2813 //GS1903// 
#define COLOR_ORDER RGB
#define NUM_STRIPS  4
#define NUM_LEDS_PER_STRIP 300
#define NUM_LEDS NUM_LEDS_PER_STRIP * NUM_STRIPS
CRGB leds[NUM_LEDS];

// EthernetUDP ddpSocket;
// byte ddpBuffer[DDP_MAX_DATALEN+DDP_HEADER_LEN];
// byte ddpFrameBuff[NUM_LEDS*3];

// LED animation stuff
ColorParam colorA;
ColorParam colorB;
ColorParam colorC;
ColorParam colorD;
#define UNIFORM_COUNT 4
FloatParam uni[UNIFORM_COUNT];
FloatParam speedParam;


IntParam brightnessParam;
BoolParam enableTestParam;
IntParam testAnimationParam;
BoolParam ledParam;
BoolParam statusLedParam;

FloatParam remoteAnimationSpeedParam;
FloatParam spiralFreqParam;

// Shadel
Pixel pixels[NUM_LEDS];
PixelStrip strip;

// Input
#define BUTTON_COUNT 3
#define POT_COUNT 3
#define POT_WINDOW_SIZE 6

const uint8_t buttonPins[BUTTON_COUNT] =  {BUT_A_PIN, BUT_B_PIN, BUT_C_PIN}; 
Bounce buttonBounce[BUTTON_COUNT];
typedef struct {
    uint16_t readings[POT_WINDOW_SIZE];
    uint16_t value;
    uint8_t pin;
} PotAvg;
int potAvgIdx = 0;
PotAvg potAvg[POT_COUNT];
#define PARAM_LINKER_COUNT 10
ParamLinker linkedParams[PARAM_LINKER_COUNT];


void ledCallback(bool _b){
    digitalWrite(LED_PIN, _b);
}
void updateRemoteSpeed(float _f){
    myMicroOscUdp.sendFloat(remoteAnimationSpeedParam.address, _f);
}

void updateSpiralFreq(float _f){
    myMicroOscUdp.sendFloat(remoteAnimationSpeedParam.address, _f);
}

void setup() {
    Serial.begin(9600);
    printer.begin();

    pinMode(LED_PIN, OUTPUT);
    digitalWrite(LED_PIN, HIGH);

    potAvg[0].pin = POT_A_PIN;
    potAvg[1].pin = POT_B_PIN;
    potAvg[2].pin = POT_C_PIN;


    remoteAnimationSpeedParam.set("/animation/speed", 0, 1, 0.5);
    paramCollector.add(&remoteAnimationSpeedParam);

    spiralFreqParam.set("/animation/spiral/freq", 0, 10, 1.0);
    paramCollector.add(&spiralFreqParam);

    statusLedParam.set("/led",0);
    statusLedParam.setCallback(ledCallback);
    paramCollector.add(&statusLedParam);
    uni[0].set("/shader/u/1", 0, 1, 0.5);
    uni[0].saveType = SAVE_ON_REQUEST;
    uni[1].set("/shader/u/2", 0, 1, 0.5);
    uni[1].saveType = SAVE_ON_REQUEST;
    uni[2].set("/shader/u/3", 0, 1, 0.5);
    uni[2].saveType = SAVE_ON_REQUEST;
    uni[3].set("/shader/u/4", 0, 1, 0.5);
    uni[3].saveType = SAVE_ON_REQUEST;
    for(int i = 0; i < UNIFORM_COUNT; i++){
        paramCollector.add(&uni[i]);
    }



    long int c = 255;//CRGB(255,0,0);
    colorA.set("/color/a", c);
    colorA.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&colorA);

    colorB.set("/color/b", c);
    colorB.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&colorB);
    
    colorC.set("/color/c", c);
    colorC.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&colorC);

 
    colorD.set("/color/d", c);
    colorD.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&colorD);
    // ledParam.set("/test/led", 1);
    // paramCollector.add(&ledParam);

    speedParam.set("/animation/speed", .001, 1., .3);
    speedParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&speedParam);



    enableTestParam.set("/leds/test/en", 0);
    enableTestParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&enableTestParam);
    testAnimationParam.set("/leds/test/anim", 0, 6, 0);
    testAnimationParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&testAnimationParam);


    brightnessParam.set("/leds/brightness", 0, 255, 200);
    brightnessParam.saveType = SAVE_ON_REQUEST;
    brightnessParam.setCallback(setBrightness);
    paramCollector.add(&brightnessParam);

    linkedParams[0].paramAddress.set("/input/pots/a", "/leds/brightness");
    linkedParams[1].paramAddress.set("/input/pots/b", "/leds/test/anim");
    linkedParams[2].paramAddress.set("/input/pots/c", "none");
    linkedParams[3].paramAddress.set("/input/buts/a", "/leds/test/en");
    linkedParams[4].paramAddress.set("/input/buts/b", "none");
    linkedParams[5].paramAddress.set("/input/buts/c", "none");
    for(int i = 0; i < PARAM_LINKER_COUNT; i++){
        linkedParams[i].paramAddress.saveType = SAVE_ON_REQUEST;
        paramCollector.add(&linkedParams[i].paramAddress);
    }
    for(int i = 0; i < BUTTON_COUNT; i++){
        buttonBounce[i].attach(buttonPins[i], INPUT_PULLUP);
        buttonBounce[i].interval(5);
    }    

    setupEsparam(&printer);
    startNetwork();
    // Ethernet.setCsPin(HSPI_CS); // set Pin 3 for CS
    // Ethernet.setRstPin(WIZ_RESET_PIN); // set Pin 4 for RST
    // // Ethernet.hardreset();
    // Ethernet.init(1);//, HSPI_CS); 
    // Ethernet.begin(mac, ip, subnet, gateway, dnsServer);
    // Ethernet.phyMode(HALF_DUPLEX_10);
    // Udp.begin(3000);
    FastLED.addLeds<LED_TYPE, LED_DATA_1_PIN, COLOR_ORDER>(leds, 0, NUM_LEDS_PER_STRIP);
    FastLED.addLeds<LED_TYPE, LED_DATA_2_PIN, COLOR_ORDER>(leds, NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);
    FastLED.addLeds<LED_TYPE, LED_DATA_3_PIN, COLOR_ORDER>(leds, 2 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);
    FastLED.addLeds<LED_TYPE, LED_DATA_4_PIN, COLOR_ORDER>(leds, 3 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);


    // shadel setup
    printer.println("[shd] setup");    
    
    shadelPrecalc();
    strip.pg.bindPixels(pixels, NUM_LEDS, 0, NUM_LEDS);
    Vec2 pointA = Vec2(1.0, 0.0);
    Vec2 pointB = Vec2(0.0, PI);
    strip.map(pointA, pointB);
    
    // artnetBegin();
    // ddpSocket.begin(DDP_PORT);
    digitalWrite(LED_PIN, LOW);
    if(esparamIsAP() == false){
        sprintf(printer.headerAText, "%s-%s ", getDeviceName(), getDeviceSSID());
        sprintf(printer.headerBText, "%s ", getDeviceIp());
    }
    else {
        sprintf(printer.headerAText, "%s-%s ", getDeviceName(), "AP-mode");
        sprintf(printer.headerBText, "%s ", getDeviceIp());
    }
    printer.println("done setup");    
    remoteAnimationSpeedParam.setCallback(updateRemoteSpeed);
    spiralFreqParam.setCallback(updateSpiralFreq);
}


void updateInput(){
    for(int i = 0;i < POT_COUNT; i++){
        potAvg[i].readings[potAvgIdx] = analogRead(potAvg[i].pin);
        if(potAvgIdx%2==0){
            potAvg[i].value = 0;
            for(int j = 0; j < POT_WINDOW_SIZE; j++){
                potAvg[i].value+=potAvg[i].readings[j];
            }
            potAvg[i].value /= POT_WINDOW_SIZE;
            // if(i==0){
            //     Serial.printf("%04i %04i\n", potAvg[i].value, analogRead(potAvg[i].pin));
            // }
        }
        buttonBounce[i].update();
    }
    potAvgIdx++;
    potAvgIdx%= POT_WINDOW_SIZE;
}



void updateLinkedParam(){
    for(int i = 0; i < PARAM_LINKER_COUNT; i++){
        if(linkedParams[i].needsUpdate()){
            if(linkedParams[i].findParam(&paramCollector)){
                // printer.printf("%s found \n %s\n",
                //     linkedParams[i].paramAddress.address,
                //     linkedParams[i].targetParam->address
                // );
            }
        }
    }
    for(int i = 0; i < PARAM_LINKER_COUNT; i++){
        if(linkedParams[i].isLinked()){
            if(i<3){
                linkedParams[i].receiveByte(map(potAvg[i].value, 0, 4095, 0, 255));
            }
            else {
                if(buttonBounce[i-3].changed()){
                    // printer.println(buttonBounce[i-3].read());
                    // linkedParams[i-3].receiveBool(buttonBounce[i-3].read());
                    if(buttonBounce[i-3].read() == 0){
                        linkedParams[i].receiveBang();
                    }
                }
            }
        }
    }
}

byte buff[1024];
unsigned long tstamp = micros();

int pcount = 0;
int fpsCounter = 0;
int fpsTStamp = 0;

float ttime = 0;

CRGB shader(Vec2 _pos, CRGB _c){
    float f = sinLook(_pos.x*_pos.x+ttime*.54+_pos.y)*.5+.5;
    f = smoothstep(0.6,1.,f); 
    CRGB out = blend(_c, colorB.v, f*255);
    return blend(_c, out, 255);
}

void setBrightness(int _b){
    FastLED.setBrightness(_b);
}

void loop() {
    // digitalWrite(LED_PIN, (millis()%63)<20);
    updateInput();
    updateLinkedParam();
    updateEsparam();
    // update the oled screen and weblog
    printer.update();

    if(enableTestParam.v){
        int i = 0;
        int v = testAnimationParam.v;
        switch(testAnimationParam.v){
            case 0:
            case 1:
            case 2:
            {
                for(i;i<NUM_LEDS;i++)
                    leds[i] = CRGB(int(v==0)*255,int(v==1)*255,int(v==2)*255);
                break;
            }
            case 3:
            {
                for(i;i<NUM_LEDS;i++)
                    leds[i] = CHSV(int(millis()*.1+i)%255,255,255);
                break;
            }
            case 4:
            {
                for(i;i<NUM_LEDS;i++) 
                    leds[i] = ((i+millis()/100)%15 == 0) ? CRGB::White : CRGB::Black;
                break;                
            }
            case 5:
            {
                for(i;i<NUM_LEDS;i++) 
                    leds[i] = CRGB::White;
                break;                
            }
        }
        FastLED.show();
    }
    else {
        ttime += speedParam.v*.08;
        strip.pg.setColor(CRGB(50,0,0));
        strip.pg.applyShader(shader);
        strip.pg.applyToLeds(leds);
        FastLED.show();
    }
}